
#!/usr/bin/env python
import sys
import subprocess
import time

#""id2name": ""

bam = sys.argv[1]
path = "/hpc/local/CentOS7/gen/data/genomes/human/gencode/"
catLocation =  {"prot": f"{path}gencode26-protein_coding+ERCC.gtf",
               "clus": f"{path}gencode26-protein_coding-clusters.gtf",
               "antiLincRNA": f"{path}gencode26-antisense+lincRNA.gtf",
               "rest": f"{path}gencode26-rest.gtf",
               "genome" : f"{path}STARindex_PARmask",}
orderedCategories = ['prot', 'clus', 'antiLincRNA', 'rest']

#----------------------------------------------------------------------
def runFastqc():
    """run fastqc on input fastqs before processing"""
    print('executing fastQC', file = sys.stderr, flush= True)
    command = f'mkdir fastqc; fastqc -o fastqc {bam}_1.fastq.gz {bam}_2.fastq.gz '
    subprocess.check_output(command, shell = True)

#----------------------------------------------------------------------
def runCutadapt():
    """"""
    print('executing adaptor trimming', file = sys.stderr, flush= True)
    command = (f'cutadapt '
               f'-a GATCGGAAGAGCACACGTCTGAACTCCAGTCACATCACGATCTCGTATGCCGTCTTCTGCTTG '
               f'-A GATCGGAAGAGCACACGTCTGAACTCCAGTCACATCACGATCTCGTATGCCGTCTTCTGCTTG '
               f'-o {bam}_cutadapt_1.fastq.gz '
               f'-p {bam}_cutadapt_2.fastq.gz '
               f'-m 20 '
               f'{bam}_1.fastq.gz {bam}_2.fastq.gz')
    subprocess.check_output(command, shell = True)

#----------------------------------------------------------------------
def runCompTrim():
    """"""
    print('executing first round of complexity Trimming', file = sys.stderr, flush= True)
    command1 = (f'multiCompTrim.py -g -l 2000000 '
                f'{bam}_cutadapt_1.fastq.gz '
                f'| gzip -n >{bam}_cutadapt_comptrim_1.fastq.gz')
    command2 = (f'multiCompTrim.py -g '
                f'{bam}_cutadapt_2.fastq.gz '
                f'| gzip -n  >{bam}_cutadapt_comptrim_2.fastq.gz ')
    subprocess.check_output(command1, shell = True)
    print('executing second round of complexity Trimming', file = sys.stderr, flush= True)
    subprocess.check_output(command2, shell = True)


#----------------------------------------------------------------------
def runSTAR():
    """run STAR """
    print('executing STAR mapping', file = sys.stderr, flush= True)
    command = (f"STAR.sh "
               f"--genomeDir {catLocation['genome']} "
               f"--runThreadN 16 --twopassMode Basic --alignEndsType Local "
               f"--alignSoftClipAtReferenceEnds yes --outSAMunmapped Within --alignSJoverhangMin 5 "
               f"--readFilesCommand zcat --outStd BAM_Unsorted --outSAMtype BAM Unsorted "
               f"--readFilesIn {bam}_cutadapt_1.fastq.gz {bam}_cutadapt_2.fastq.gz  "
               f"> {bam}.bam 2> {bam}.STAR.elog")
    subprocess.check_output(command, shell = True)

#----------------------------------------------
def featureCounts():
    """call featurecounts for all categories"""
    timeStart = time.time()
    print('executing featureCounts', file = sys.stderr, flush= True)
    for category in orderedCategories:
        command = (f'featureCounts -s 2 -a {catLocation[category]} '
                   f'-p -R -B -o {category}.featureCounts.out {bam}.bam && '
                   f'mv {bam}.bam.featureCounts {category}.{bam}.featureCounts')
        subprocess.check_output(command, shell = True)
    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)

#----------------------------------------------------------------------
def getReadsList():
    """get list of all read names out of featurecounts output"""
    timeStart = time.time()
    print('getting readlists', file = sys.stderr, flush = True)
    with open(f'prot.{bam}.featureCounts', 'r') as reads:
        for line in reads:
            readName = line.split('\t')[0]
            yield readName
    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)

#----------------------------------------------------------------------
def filterFile(fileName):
    """"""
    timeStart = time.time()
    print(f"filtering {fileName}", file = sys.stderr, flush = True)
    command = f'awk \'{{if ($3 != "*") print $0}}\' {fileName} > filteredFC.txt'
    subprocess.check_output(command, shell = True)
    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)

    return open('filteredFC.txt', 'r')

#----------------------------------------------------------------------
def makeCountTable():
    """"""
    print('making count table', file = sys.stderr, flush = True)
    rawCountTable = {}
    processedReads = set()
    totalReadCount = 0
    timeStart = time.time()

    for category in orderedCategories:
        with filterFile(f'{category}.{bam}.featureCounts') as featOutput:
            print(f'processing {category}', file = sys.stderr, flush= True)
            timeStartIn = time.time()
            for line in featOutput:
                tempList = line.split('\t')
                readName = tempList[0]
                assignement = tempList[2]

                if readName not in processedReads :  #and assignement != '*'
                    #increase feature counts
                    if assignement not in rawCountTable:
                        rawCountTable[assignement] = 1
                    else:
                        rawCountTable[assignement] += 1

                    processedReads.add(readName)

                    #upping read count
                    totalReadCount += 1
                    if totalReadCount % 100000 == 0:
                        print(f'{totalReadCount} done, time: {time.time() - timeStartIn}',
                              file = sys.stderr, flush = True)
                        timeStartIn = time.time()

            print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)

    return [rawCountTable, totalReadCount]


#----------------------------------------------------------------------
def getLengths():
    """"""
    timeStart = time.time()
    print(f'gathering gene lengths', file = sys.stderr)
    lengths = {}

    for category in orderedCategories:
        with open(f'{category}.featureCounts.out', 'r') as lengthsFile:
            next(lengthsFile)
            next(lengthsFile)
            for line in lengthsFile:
                tempList = line.split('\t')
                featureName = tempList[0]
                length = tempList[5]

                lengths[featureName] = length

    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)
    return lengths


#----------------------------------------------------------------------
def convertToTPM(rawCountTable, totalReadCount, geneLengths):
    """"""
    print(f'converting to TPM', file = sys.stderr)
    timeStart = time.time()
    TPK = {name: int(value) / (float(geneLengths[name]) / float(1000)) for (name, value) in rawCountTable.items()}
    totTPK = sum(TPK.values())
    totTPKPerMilion = totTPK / 1000000
    TPM = {name: float(value) / totTPKPerMilion for name, value in TPK.items()}

    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)
    return TPM


#----------------------------------------------------------------------
def convertNames(TPMTable):
    """"""
    timeStart = time.time()
    print(f'setting names right', file = sys.stderr)
    id2NamePath = "/hpc/local/CentOS7/gen/data/genomes/human/gencode/gencode.v26-id2name.txt"
    id2NameDict = {}
    with open(id2NamePath, 'r') as id2Name:
        for line in id2Name:
            line = line.strip()
            tempList = line.split("\t")
            geneID = tempList[0]
            name = tempList[1]

            id2NameDict[geneID] = f"{geneID}__{name}"


    #actually convertNames
    finalTable = {}
    for geneID in TPMTable:
        tmpList = geneID.split('.')
        geneIDTrimmed = tmpList[0]
        if geneIDTrimmed in id2NameDict:
            finalTable[id2NameDict[geneIDTrimmed]] = TPMTable[geneID]
        elif 'cluster' in geneIDTrimmed:
            finalTable[geneIDTrimmed] = TPMTable[geneIDTrimmed]
        else:
            print(f'expressed gene has not corresponding symbol: {geneID}', file = sys.stderr,
                  flush = True)

    print(f'done, time: {time.time() - timeStart}', file = sys.stderr, flush = True)
    return finalTable



#main
runFastqc()
runCutadapt()
#runCompTrim()
runSTAR()
featureCounts()
rawCountTable, totalReadCount = makeCountTable()
lengths = getLengths()
TPMTable = convertToTPM(rawCountTable, totalReadCount, lengths)
finalTable = convertNames(TPMTable)


for key in sorted(finalTable.keys()):
    print(f'{key}\t{finalTable[key]}')


