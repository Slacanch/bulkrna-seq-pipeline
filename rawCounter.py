import sys

fcFilePath = sys.argv[1]
fcFile = open(fcFilePath, 'r')

countDict = {}

#counting ID occurrences
for line in fcFile:
    tempList = line.strip().split()
    ensID = tempList[2]

    if ensID == '*':
        continue

    if ensID in countDict:
        countDict[ensID] += 1
    else:
        countDict[ensID] = 1


for key in sorted(countDict.keys()):
    print(f"{key}\t{countDict[key]}")

 ##setting names right?

#print(f'setting names right', file = sys.stderr)
#id2NamePath = "/hpc/local/CentOS7/gen/data/genomes/human/gencode/gencode.v26-id2name.txt"
#id2NameDict = {}
#with open(id2NamePath, 'r') as id2Name:
    #for line in id2Name:
        #line = line.strip()
        #tempList = line.split("\t")
        #geneID = tempList[0]
        #name = tempList[1]

        #id2NameDict[geneID] = f"{geneID}__{name}"







